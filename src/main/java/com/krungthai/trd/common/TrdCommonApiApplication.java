package com.krungthai.trd.common;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrdCommonApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrdCommonApiApplication.class, args);
	}

}
